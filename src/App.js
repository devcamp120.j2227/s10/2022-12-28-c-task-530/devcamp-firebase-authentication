import logo from './logo.svg';
import './App.css';

import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup, signOut } from "firebase/auth";
import auth from './firebase';
import { useEffect, useState } from 'react';

const provider = new GoogleAuthProvider();

function App() {
  const [user, setUser] = useState(null);

  const loginGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        console.log(result);
        setUser(result.user);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  const logoutGoogle = () => {
    signOut(auth)
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      console.log(result);
      setUser(result);
    })
  })

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          user ?
            <>
              <p>
                Hello, {user.displayName}
              </p>
              <img src={user.photoURL} width={50} style={{borderRadius: "50%", marginBottom: "20px"}}/>
              <button onClick={logoutGoogle}>Sign out</button>
            </>
            

            :
            <>
              <p>
                Please Sign In
              </p>
              <button onClick={loginGoogle}>Sign in with Google</button>
            </>
        }
        
      </header>
    </div>
  );
}

export default App;

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDZoK4s_yhZRHMerSiAW1FR7N8HbD_o608",
  authDomain: "devcamp-r27.firebaseapp.com",
  projectId: "devcamp-r27",
  storageBucket: "devcamp-r27.appspot.com",
  messagingSenderId: "31018431823",
  appId: "1:31018431823:web:a7613caf7d1df75d8e013a"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;